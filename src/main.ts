import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import{NextFunction, Request,Response} from 'express'
import { ValidationPipe } from '@nestjs/common';

function globalmiddleware(req:Request,res:Response, next :NextFunction){
  console.log('this is the global middleware first');
  next();
}
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.use(globalmiddleware);
  const  port = await app.listen(3000);
}
bootstrap();
