import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import{ConfigModule}from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { AdminModule } from './admin/admin.module';
import * as mongoose from 'mongoose';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './users/db/user.schema';



@Module({
  imports: [ConfigModule.forRoot(),
    UsersModule,
     AdminModule ,
     MongooseModule.forFeature([{name:'user' , schema:UserSchema}])
     
    
    ],
  controllers: [AppController ],
  providers: [AppService],
})
export class AppModule {}
