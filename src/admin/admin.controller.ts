import { Controller, Get, HttpStatus, HttpCode, Post, Body, Delete, Param, Put } from '@nestjs/common';
import { AdminService } from './admin.service';
import {admin  } from './admin .dta'

@Controller('admin')
export class AdminController {
    constructor (private Service: AdminService){}
     @Post('/registeradmin')
    @HttpCode(HttpStatus.CREATED)
    register(@Body() body : admin):string{
        return this.Service.addNewAdmin(body);
    }
    @Put('/updateadmin')
    @HttpCode(HttpStatus.CONTINUE)
    update(@Body() body:admin):string{
       return this.Service.updateAdmin(body);
    }
    @Get('/findadmin')
    @HttpCode(HttpStatus.ACCEPTED)
    findbook():admin[]{
        return this.Service.findallAdmin();
    }
    @Delete('/deleteadmin/:id')
    @HttpCode(HttpStatus.ACCEPTED)
    Delete(@Param('id') bookid :string){
        return this.Service.deleteAdmin(bookid );
    }
    
}

   



