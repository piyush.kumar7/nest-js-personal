import { Injectable } from '@nestjs/common';
import { admin } from './admin .dta'

@Injectable()
export class AdminService {
    public admin: admin[] = []
    addNewAdmin(book: admin): string {
        this.admin.push(book)
        return "book has been added"
    }
    updateAdmin(book: admin): string {
        let index = this.admin.findIndex((currentbook) => {
            return currentbook.id === book.id;

        })
        this.admin[index] = book;
        return 'book has been updated'
    }
    deleteAdmin(bookid: String): string {
        this.admin = this.admin.filter((book) => {
            return bookid != bookid;
        });
        return 'book has been deleted'

    }
    findallAdmin(): admin[] {
        return this.admin;
    }
}
