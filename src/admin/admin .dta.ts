export interface admin {
    
    id: string;
    title: string;
    author: string;
    published: number
}