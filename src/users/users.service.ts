import { Injectable } from "@nestjs/common";
import { Model } from "mongoose";
import { UserSchema, UserDocument, user } from "./db/user.schema";
import { InjectModel } from "@nestjs/mongoose";

// const data = process.env;
@Injectable()
export class UsersService {
  user: any;
  // public users: user[] = [];
  constructor(
    // @InjectModel("user") private readonly usermodel: Model<UserDocument>
  ) {}

  RegisterUser(name :user) {
    this.user.push(name);
    return "registered successfully";
  }
  FindAll(): user[] {
    return this.user;
  }
  update(userid): string {
    let userindex = this.user.findIndex((newuser) => {
      return newuser.id === userid.id;
    });
    this.user[userindex] = user; 

    return "user details updated";
  }
  removeUser(userid = String): string {
    this.user = this.user.filter((user) => {
      return userid != userid;
    });

    return "user remove  ";
  }
}
