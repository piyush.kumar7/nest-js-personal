import{isEmail, isNotEmpty, min, MinLength,} from 'class-validator';
import { Prop, Schema, SchemaFactory, } from '@nestjs/mongoose/dist';
import { HydratedDocument } from 'mongoose';


export type UserDocument = HydratedDocument<user>;
@Schema()
export class user {
    @MinLength(1)
    id:number
    
    @MinLength(6)
    name:String
    @MinLength(10)
    mobilenumber:Number
    @Prop()
    email:String
    
    @Prop({default:Date.now})
    date : Date
}
export const UserSchema = SchemaFactory.createForClass(user);