import { UserSchema, user } from './user.schema';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {  UsersController } from '../users.controller';
import { UsersService } from '../users.service';


@Module({
  imports: [MongooseModule.forFeature([{ name: user.name, schema: UserSchema }])],
  controllers: [UsersController],
  providers: [UsersService],
})
export class usersModel {}