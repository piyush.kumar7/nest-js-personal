import { Controller, Get, HttpCode, HttpStatus, Post, Param, Res, Patch, Body, Delete, Req, UsePipes, ValidationPipe, Type, Put } from '@nestjs/common';
import { UsersService } from './users.service';
import { user } from './db/user.schema';
import{Request,Response,} from 'express'


@Controller('users')
export class UsersController {
    constructor(private  Service: UsersService) {}
    @Post('/register')
    @UsePipes( ValidationPipe)
    @HttpCode(HttpStatus.CREATED)
    Registration(@Body()  name:user){
        return this.Service.RegisterUser(name);
    }
    @Get('/search/:id')
    @HttpCode(HttpStatus.ACCEPTED)
    FindAll(@Param('id') id:string,@Res() res:Response){
        
        return this.Service.FindAll();
     }
     @Put('/update/:id')
     update(@Body() user: string,@Param('id') userid: number  ) {
       return this.Service.update(userid);
     }
     @Delete(':id')
     remove(@Param('id') userid) {
       return this.Service.removeUser(userid);
    }

   

}
