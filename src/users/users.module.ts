import { Module, Controller } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { DbModule } from './db/db.module';
import { usersModel } from './db/users.model';



@Module({
  imports:[DbModule,usersModel],
  providers: [UsersService],
  controllers:[UsersController]
})
export class UsersModule {}
